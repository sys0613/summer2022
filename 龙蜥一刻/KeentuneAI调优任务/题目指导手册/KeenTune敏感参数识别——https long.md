# KeenTune敏感参数识别——https long
操作系统内核参数和应用参数庞大的数量对参数智能调优带来了极大的挑战，使用敏感参数识别算法对参数进行筛选可以大大降低参数的规模，提高那参数智能调优的效率和准确性。KeenTune中内置了很多敏感参数识别算法，例如：lasso，稀疏化线性敏感参数识别算法；univariate，基于互信息(mutual information)的单一敏感参数识别算法；shap，基于博弈论的非线性敏感参数识别算法。
在本题目中，我们为你提供了真实环境下的采样数据，希望你使用KeenTune利用采样数据分析http long场景下的内核参数的敏感性，为参数调优提供指导。

## 1. KeenTune安装和配置
KeenTune总共包含四个组件：keentuned，keentune-target，keentune-brain，keentune-bench，在专家调优实践任务中，我们只需要安装keentune-brain和keentuned两个组件，并且只需要一台虚拟机来完成这个任务。
我们可以选择使用YUM或者源码安装的方式来下载和安装KeenTune的组件，具体步骤请参考<a href="../keentune_docs/KeenTune_install.md" target="_blank">《Keentune安装手册》</a>

## 2. 数据集下载
我们为你提供了实验所需的全部数据，你可以从<a href="../datas/demo-https-long.tar.gz" target="_blank">下载链接</a>处下载。
数据文件为.pkl格式，都包含以下几个文件（本小节内容不影响实验的进行，仅作简单介绍，可以跳过）   

+ bench.pkl，benchmark工具的配置文件
+ knobs.pkl，参数配置文件
+ score.pkl，性能得分
+ points.pkl，参数取值
+ time.pkl，运行耗时
+ loss.pkl/loss_parts.pkl，loss数据  

敏感参数识别的原理是根据参数配置组合和其对应的性能得分，通过敏感分析算法，找出对性能得分影响较大的参数配置。


## 3. KeenTune参数敏感分析
首先我们将解压后的**数据集文件夹**copy到/var/keentune/data/tuning_data/tuning路径下(没有可以手动创建)，这个路径也是KeenTune运行智能参数调优时保存文件的路径。
![数据文件存放位置（注意：请根据题目要求选择数据文件）](assets/demo_data.jpg)
我们应该可以通过keentune sensitize list命令看到我们提供的文件.   
![](assets/44.png)  
查到数据文件之后，通过下面这个命令使用KeenTune进行敏感参数识别，更具体的步骤请参考<a href="../keentune_docs/KeenTune_sensitize.md" target="_blank">《KeenTune敏感参数识别手册》</a>

```shell
keentune sensitize train --data demo-https-long --output demo-https-long-result --trials 2
```
![](assets/55.png)  


## 4. 结果提交
等待算法运行完成，算法运行的结果会保存成一个json文件，我们希望你将这个json文件提交到系统中以证明你完成了这项任务  
敏感参数识别结果文件如下所示
```json
{
    "nginx": {
        "ssl_session_cache@group-1": {
            "weight": 0.010637188
        }
    },
    "sysctl": {
        "fs.suid_dumpable@group-1": {
            "weight": 0.04909493
        },
        "kernel.sched_autogroup_enabled@group-1": {
            "weight": 0.046569254
        }
    }
}
```
提交流程参考<a href="../KeenTune%E4%BB%BB%E5%8A%A1%E9%AA%8C%E6%94%B6%E6%B5%81%E7%A8%8B.md" target="_blank">《任务验收流程》</a>

---    

## 常见问题
+ yum源安装找不到组件  
修改`/etc/yum.repos.d`目录下的文件，增加以下内容
```conf
[KeenTune]
baseurl=https://mirrors.openanolis.cn/anolis/8.6/Plus/$basearch/os
enabled=1
gpgkey=https://mirrors.openanolis.cn/anolis/RPM-GPG-KEY-ANOLIS
gpgcheck=0
```

+ 我找不到我的任务日志文件了  
所有的日志文件都保存在`/var/log/keentune`路径下

+ 如何选择使用的敏感参数算法  
修改`/etc/keentune/conf/brain.conf`文件中的sensi->explainer配置项，选择gp, lasso, univariate, shap其中一项，修改后重启keentune-brain组件，注意提前安装好依赖
```shell
systemctl restart keentune-brain
```

+ 在哪能找到敏感参数识别的结果文件  
文件夹`/var/keentune/sensitize/`下可以找到对应的json文件，日志中也会告诉你具体路径
