# KeenTune 安装配置手册
## YUM安装
参考[这里](KeenTune_requirements.md)安装KeenTune所需的所有依赖，然后使用以下命令安装KeenTune各组件
```s
#yum源安装keentune各组件
yum install keentuned keentune-brain keentune-bench keentune-target
```

参照[这里](KeenTune_config.md)编辑keentuned配置文件，然后启动各组件（可以使用tmux，nohub或多个窗口运行的方式启动）

```s
# 启动keentuned服务
systemctl start keentuned
# 启动keentune-brain服务
systemctl start keentune-brain
# 启动keentune-bench服务
systemctl start keentune-bench
# 启动keentune-target服务
systemctl start keentune-target
```

## 源码安装
使用以下命令下载KeenTune的源代码  
```s
git clone https://gitee.com/anolis/keentuned.git
git clone https://gitee.com/anolis/keentune_brain.git
git clone https://gitee.com/anolis/keentune_target.git
git clone https://gitee.com/anolis/keentune_bench.git
```

参考[这里](KeenTune_requirements.md)安装KeenTune所需的所有依赖，然后使用以下命令编译和启动KeenTune各组件

编译安装各组件（在各组件git目录下分别执行以下命令）
+ keentuned
```s
cd keentuned
./keentuned_install.sh
```
+ keentune-brain
```s
cd keentune_brain
sudo python3 setup.py install
```
+ keentune-target
```s
cd keentune_target
sudo python3 setup.py install
```
+ keentune-bench
```s
cd keentune_bench
sudo python3 setup.py install
```

参照[这里](KeenTune_config.md)编辑keentuned配置文件，然后启动各组件（可以使用tmux，nohub或多个窗口运行的方式启动）

```s
# 启动keentuned服务
keentuned
# 或者
nohup keentuned > keentuned.log 2>&1 &

# 启动keentune-brain服务
keentune-brain
# 或者
nohup keentune-brain > brain.log 2>&1 &

# 启动keentune-bench服务
keentune-bench
# 或者
nohup keentune-bench > bench.log 2>&1 &

# 启动keentune-target服务
keentune-target
# 或者
nohup keentune-target > target.log 2>&1 &
```