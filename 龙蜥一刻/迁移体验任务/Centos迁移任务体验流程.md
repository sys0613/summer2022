没有CentOS系统进行迁移体验？不要急，**龙蜥实验室**来啦！

## CentOS替代背景

> - 2020年12月08日CentOS官方宣布CentOS项目将停止，并推出CentOS Stream项目，详见公告。
> - CentOS未来将会从 RedHat Enterprise Linux(RHEL) 复刻版本的 CentOS Linux 转向 CentOS Stream。
> - 对于当前处于生命周期中的 CentOS 版本后续影响：
>        - CentOS Linux 7（简称CentOS 7）将会延续当前的支持计划，于2020年第四季度停止更新，并于2024年6月30日停止维护(EOL，End Of Life)。
>        - CentOS官方宣布CentOS Linux 8（简称CentOS 8）将在2021年底停止维护。
> - 因此当前使用CentOS系统的用户面临停服后如何更新、维护、系统迁移等问题。

详细的迁移背景可以参考<a href="https://openanolis.cn/centos-eol#Background" target="_blank">龙蜥社区CentOS替代专区</a>。

## 操作步骤

在龙蜥社区官网活动页领取任务后，系统会自动创建一个任务（Issue），前往 Gitee 任务页查看任务内容，开始使用免费资源，体验从CentOS迁移到龙蜥操作系统（Anolis OS）！

### 第一步：记录你的任务 ID

在开始执行任务前，在 Gitee 任务页查看并记录任务 ID（即IssueID），后续提 PR 时需要使用此 ID。

### 第二步：申请免费资源-龙蜥实验室

龙蜥实验室为社区用户提供了一个预装龙蜥OS的在线机器资源服务；用户可以通过web页面及机器人等形式自动创建和管理机器资源，为社区用户体验Anolis OS以及使用Anolis OS进行开发测试提供了极大的便利性，满足社区用户对于机器资源的各类需求。

#### 使用指南文档
<a href="https://www.yuque.com/anolis-docs/community/peng85" target="_blank">https://www.yuque.com/anolis-docs/community/peng85</a>

#### 申请机器
为了用户更加方便地体验龙蜥OS，龙蜥实验室免费为社区用户提供OS镜像实例，在迁移任务中可以使用龙蜥实验室完成迁移工作。
<a href="https://lab.openanolis.cn/#/apply/home" target="_blank">**开始体验吧！**</a>

### 第三步：参考操作系统迁移手册，体验迁移

Anolis OS提供了三种场景的迁移适配工作，申领任务后，可以根据对应的手册完成迁移任务：

● <a href="https://openanolis.cn/sig/migration/doc/451732372594279514" target="_blank">CentOS 7迁移Anolis OS 7手册</a>

● <a href="https://openanolis.cn/sig/migration/doc/380658406925893825" target="_blank">CentOS 8迁移Anolis OS 8手册</a>

● <a href="https://openanolis.cn/sig/migration/doc/447499505912234337" target="_blank">CentOS 7迁移Anolis OS 8手册</a>


### 第四步：提交任务日志

完成CentOS迁移体验以后，需要将日志文件提交到任务仓库，以便于完成任务。体验CentOS迁移的任务仓库在[这里](https://gitee.com/anolis-challenge/activity-lab-migrate)。

#### Fork结果仓库
# ![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2022/png/277043/1655279568265-ba77addb-fe34-4281-8c9c-a627104dbc84.png#clientId=ubaae3021-25cf-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=310&id=uee70c296&margin=%5Bobject%20Object%5D&name=image.png&originHeight=620&originWidth=2280&originalType=binary&ratio=1&rotation=0&showTitle=false&size=420835&status=done&style=none&taskId=u53d11173-5e11-4b4e-99ea-5eb957e4dc0&title=&width=1140)

#### 克隆结果仓库

1. 首先下载结果提交仓库：
```bash
git clone git@gitee.com:<gitee account name>/activity-lab-migrate.git

```

2. 在结果仓库中，执行如下命令创建自己的目录。
```bash
cd activity-lab-migrate
mkdir -pv <gitee account name>
cd ..

```
3. 以gitee账号名作为目录名称，建好待用，返回到上级目录。

#### 提交结果

迁移过程结束后，迁移工具会生成一个日志文件（*一般为/var/log/centos2anolis.log 或 /var/log/leapp/leapp-upgrade.log*）。
![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2022/png/277043/1655279847456-c234ba27-b77e-4b88-b010-bdadfae6af0a.png?x-oss-process=image/format,png#clientId=ubaae3021-25cf-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=128&id=u80af444c&margin=%5Bobject%20Object%5D&name=image.png&originHeight=256&originWidth=981&originalType=binary&ratio=1&rotation=0&showTitle=false&size=757750&status=done&style=none&taskId=ub8acf124-085b-4cd4-97db-05f6c204530&title=&width=490.5)
![image.png](asserts/migration-log_centos7.png)

进入代码仓库，提交刚刚的修改。

```bash
#进入结果仓库
cd activity-lab-migrate

#建立一个提交结果的新分支
git checkout -b <username>/add-migration-result

#进入用户自己的结果目录
cd <gitee account name>

#复制迁移过程文件到当前目录，文件路径要结合实际情况
cp /var/log/centos2anolis.log .

#暂存目录的最新修改
git add .

#提交新文件,并编写commit log
git commit -sv
```

在commit log中加入任务的链接。
```bash
Submit migration result

<ISSUE LINK>

加入后的样例如下，在最开始的两行加入如下内容,后面的内容维持原状：
Submit migration result
https://gitee.com/anolis-challenge/summer2022/issues/I69V8M?from=project-issue
.....
```

#### 推送文件
修改好文件后，推送。
```bash
#推送提交
git push
```

#### 创建PR（Pull Request）

推送代码后，系统会提示你如何创建一个PR（Pull request）。

注意：

![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2022/png/277043/1655225634548-f905f2e9-e867-47f2-93d3-74cd9b6d1607.png#clientId=uc3ad0a5f-b74a-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=147&id=u41016e07&margin=%5Bobject%20Object%5D&name=image.png&originHeight=294&originWidth=1412&originalType=binary&ratio=1&rotation=0&showTitle=false&size=189300&status=done&style=none&taskId=u6d0bb0f4-98c9-4605-b0b0-9f9430a7578&title=&width=706)
