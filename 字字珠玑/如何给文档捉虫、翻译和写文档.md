## 文档捉虫

### 目标
在龙蜥社区的SIG文档和CentOS停服专区文档中找bug，提升文档质量。

### 捉虫标准
接受反馈的文档问题，以及对应的贡献值，请参见<a target="_blank" href="https://gitee.com/anolis-challenge/summer2022/blob/master/%E6%B4%BB%E5%8A%A8%E7%9B%B8%E5%85%B3%E5%B8%AE%E5%8A%A9%E6%96%87%E6%A1%A3/%E6%9D%83%E7%9B%8A%E7%BB%86%E5%88%99.md#1-%E5%AD%97%E5%AD%97%E7%8F%A0%E7%8E%91%E6%96%87%E6%A1%A3%E6%8D%89%E8%99%AB">权益细则-文档捉虫</a>。

### 操作步骤

**建议：在活动页上观看1分钟<a target="_blank" href="https://openanolis.cn/community/activity/bug">视频</a>，快速上手。**

1. 第一步：打开<a target="_blank" href="https://openanolis.cn/sig">社区SIG</a>中的文档，或<a target="_blank" href="https://openanolis.cn/centos-eol">CentOS停服专区</a>中的内容。

2. 第二步：浏览文档，用鼠标选中有问题的一行或一段文字，并点击浮现的小图标。

3. 第三步：选择“文档捉虫”页签，输入文档建议后，提交Issue。

**注意：**
- 同一个帐号，如果重复提交同样的问题，只算一个问题。
- 问题描述尽可能清晰，更容易社区人员定位与解决。
- 社区人员会尽快审核收到的bug，经审核有效的bug将会发放贡献值。


## 文档翻译

### 目标
将龙蜥社区的SIG文档和CentOS停服专区文档，翻译成英文。

### 翻译标准
翻译要求，以及对应的贡献值，请参见<a target="_blank" href="https://gitee.com/anolis-challenge/summer2022/blob/master/%E6%B4%BB%E5%8A%A8%E7%9B%B8%E5%85%B3%E5%B8%AE%E5%8A%A9%E6%96%87%E6%A1%A3/%E6%9D%83%E7%9B%8A%E7%BB%86%E5%88%99.md#2-%E5%AD%97%E5%AD%97%E7%8F%A0%E7%8E%91%E6%96%87%E6%A1%A3%E7%BF%BB%E8%AF%91">权益细则-文档翻译</a>。

### 操作步骤

**建议：在活动页上观看1分钟<a target="_blank" href="https://openanolis.cn/community/activity/bug">视频</a>，快速上手。**

1. 第一步：打开<a target="_blank" href="https://openanolis.cn/sig">社区SIG</a>中的文档，或<a target="_blank" href="https://openanolis.cn/centos-eol">CentOS停服专区</a>中的内容。

2. 第二步：浏览文档，用鼠标选中有问题的一行或一段文字，并点击浮现的小图标。

3. 第三步：选择“文档翻译”页签，输入译文后，提交Issue。

**注意：**同一个帐号，如果重复提交同一段译文，只发放一次贡献值。


## 写文档

### 目标
为社区贡献文档，包括：使用社区产品/基础设施的最佳实践、FAQ、使用体验或建议反馈等。

### 内容标准

文档可以是任意一种：

- FAQ。
- 最佳实践。
- 使用社区产品的真实体验，以及建议反馈。

详细的内容要求，以及对应的贡献值，请参见<a target="_blank" href="https://gitee.com/anolis-challenge/summer2022/blob/master/%E6%B4%BB%E5%8A%A8%E7%9B%B8%E5%85%B3%E5%B8%AE%E5%8A%A9%E6%96%87%E6%A1%A3/%E6%9D%83%E7%9B%8A%E7%BB%86%E5%88%99.md#3-%E5%AD%97%E5%AD%97%E7%8F%A0%E7%8E%91%E5%86%99%E6%96%87%E6%A1%A3">权益细则-写文档</a>。

### 操作步骤

1. 第一步：建议你在本地先准备好文档。

2. 第二步：前往<a target="_blank" href="https://gitee.com/anolis-challenge/activity-common-task">提交PR的仓库</a>。

3. 提交PR。

    建议使用快捷版提交PR的方式，详细步骤请参见<a target="_blank" href="https://gitee.com/anolis-challenge/summer2022/blob/master/%E6%B4%BB%E5%8A%A8%E7%9B%B8%E5%85%B3%E5%B8%AE%E5%8A%A9%E6%96%87%E6%A1%A3/%5B%E4%BA%BA%E4%BA%BA%E9%83%BD%E5%8F%AF%E4%BB%A5%E5%8F%82%E4%B8%8E%E5%BC%80%E6%BA%90%5D%E6%B4%BB%E5%8A%A8%E7%9A%84%E5%B8%B8%E8%A7%81%E9%97%AE%E9%A2%98(FAQ).md#%E5%A6%82%E4%BD%95%E6%8F%90%E4%BA%A4pull-requestpr">如何提交PR</a>。

**注意：** 提交PR标题必须以 **#+issueID** 开头，否则无法发放贡献值。

## 常见问题
本次活动的常见问题，请参见<a target="_blank" href="https://gitee.com/anolis-challenge/summer2022/blob/master/%E6%B4%BB%E5%8A%A8%E7%9B%B8%E5%85%B3%E5%B8%AE%E5%8A%A9%E6%96%87%E6%A1%A3/%5B%E4%BA%BA%E4%BA%BA%E9%83%BD%E5%8F%AF%E4%BB%A5%E5%8F%82%E4%B8%8E%E5%BC%80%E6%BA%90%5D%E6%B4%BB%E5%8A%A8%E7%9A%84%E5%B8%B8%E8%A7%81%E9%97%AE%E9%A2%98(FAQ).md">FAQ</a>。

如果对活动有任何疑问，欢迎入群沟通：
- 加助手-小龙的微信（openanolis_assis），拉你加入微信群
- 搜索加入钉钉交流群：33311793