# 完成T-One新手试练任务

在龙蜥社区官网活动页领取任务后，系统会自动创建一个任务（Issue），前往Gitee任务页查看任务内容，然后开始体验T-One（testing in one）测试！

## 什么是T-One

怎样测试龙蜥操作系统 (Anolis OS) 的性能和功能？有的同学或许使用过 ltp，unixbench，sysbench 等常见的测试工具，有的同学可能没有接触过。使用这些测试工具来评估操作系统的性能和功能，需要了解它的使用方式，包括编译安装，执行测试，数据收集等等，有时候还需要重复执行多次，对于绝大多数人来说整个过程还是较为繁琐，你可能会想如果有个用鼠标点击几次就可以发起测试任务，并且自动收集测试结果的工具就好了。这个时候 T-One（testing in one） 就能完美解决这个需求，它还能提供一站式自动化测试集成、管理、执行、分析，以及提高跨团队、跨企业质量协作能力。

本文档，描述了如何体验T-one平台。

## 操作步骤

### 第一步：记录你的任务ID

在开始执行任务前，在Gitee任务页查看并记录任务ID（即IssueID），后续提PR时需要使用此ID。

![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2022/png/277043/1655833980319-96c2bddc-1213-4e62-9c02-ba3d3680baaa.png#clientId=ue38d38f9-da9e-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=145&id=u92128c04&margin=%5Bobject%20Object%5D&name=image.png&originHeight=290&originWidth=1240&originalType=binary&ratio=1&rotation=0&showTitle=false&size=127050&status=done&style=none&taskId=ud6898417-959f-4d41-9e5e-a685991017f&title=&width=620)

## 第二步：开始新手试练

1. 打开<a href="https://tone.openanolis.cn/" target="_blank" rel="noopener">T-One平台</a>，进入新手试练专区。

![image.png](./assets/T-One-task-01.png)

2. 申请加入新手试炼。

![image.png](./assets/T-One-task-14.png)

3. 创建任务。

    3.1 进入新手试练工作空间后，点击“新建Job”。

    ![image.png](./assets/T-One-task-02.png)<br />

    3.2 配置任务的基本信息。

    ![image.png](./assets/T-One-task-03.png)
 
4. 选择用例。

    4.1 点击“选择用例”。

    <br />![image.png](./assets/T-One-task-04.png)

    4.2 将会弹出侧边栏。新手试练区只有一个演示用例，它不会执行实际的操作系统测试动作，仅仅是用于体验T-One的工作流程。

    ![image.png](./assets/T-One-task-05.png)

    4.3 选择“flower_whisperer”用例后，点击“确定”，测试用例会被添加到用例区。

    ![image.png](./assets/T-One-task-06.png)

    4.4 点击用例右侧的“配置”按钮，为测试用例配置运行环境。

    ![image.png](./assets/T-One-task-07.png)

    4.5 在右侧跳出的边栏中做好配置，如下图所示。

    ![image.png](./assets/T-One-task-08.png)

5. 提交执行。

    配置好用例与机器环境后，就可以提交测试任务了。

    ![image.png](./assets/T-One-task-10.png)

6. 查看结果。

    6.1 提交完测试以后，平台会自动跳转到测试结果的页面，如下图所示。等待一段时间后，或者手动刷新页面，任务状态会逐步变成“运行中（Running）”和“完成（Complete）”。

    ![image.png](./assets/T-One-task-09.png)

    6.2 点击任务名称进入结果详情页面，并查看测试结果文件的细节内容。

    ![image.png](./assets/T-One-task-12.png)

    6.3 点击“stdout.log”文件，查看测试结果，并保存显示的小龙语录，将用于提交PR。

    注意：如果显示的汉字是乱码，可以调整浏览器的字符集为UTF-8。

    ![image.png](./assets/T-One-task-13.png)

7. 下一步：提交PR。

截至目前，你体验了一次测试，但还未完全完成领取的任务，必须提交PR，才能完成任务，获得贡献值。点击了解如何<a href="https://gitee.com/anolis-challenge/summer2022/blob/master/%E9%9A%8F%E6%9C%BA%E8%AF%95%E7%82%BC/T-One%E6%B5%8B%E8%AF%95/T-One%E4%BB%BB%E5%8A%A1%E9%AA%8C%E6%94%B6%E6%B5%81%E7%A8%8B.md" target="_blank">完成提交PR</a>。