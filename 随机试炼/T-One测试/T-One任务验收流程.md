当完成T-One体验任务以后，需要提交一个结果供系统检查。提交的内容是体验用例flower_whisperer的输出文本。

提PR有两种方式，任选其一：
- 轻量方式PR：直接在官方仓库中提交，适用于改动较少的时候。
- 日常方式提交PR：将官方仓库fork到本地，在本地修改后，push到你自己的仓库，然后再生成PR提交到官方仓库。

## 方式一：轻量方式提交PR

1. 打开<a href="https://gitee.com/anolis-challenge/activity-box-lab" target="_blank">T-One任务的PR官方仓库</a>。

2. 在T-One目录下，新建一个文件夹。该文件夹必须以你的Gitee账号命名，注意：不是Gitee昵称。

3. 在该目录下创建xxx.log文件，xxx.log文件内容为T-One任务结果文件stdout.log文件里的文字。注意：PR标题必须带上 #issue ID 字段。  

![](assets/01.png)  
![](assets/02.png)  
![](assets/03.png)  
![](assets/04.png)  
![](assets/05.png) 

## 方式二：日常方式提交PR

1. 打开<a href="https://gitee.com/anolis-challenge/activity-box-lab" target="_blank">T-One任务的PR官方仓库</a>，并点击fork，把官方仓库代码fork到自己的gitee仓库。

![](assets/106.png)

2. 在已fork的仓库的T-One目录下，新建一个文件夹。该文件夹必须以你的Gitee账号命名，注意：不是Gitee昵称。

3. 在该目录下创建一个xxx.log的文件。把T-One任务结果文件stdout.log文件里的文字，粘贴到文件中，并提交修改。

![](assets/06.png)   
![](assets/07.png)   

4. 在自己的gitee仓库上点击Pull Request，创建一个PR，目标分支选择 anolis-challenge/activity-box-lab，在PR的标题开头输入#issueID 模版关联issue， 在 PR body 中输入`tone_url=T-One平台上结果页面的URL链接`，然后点击提交，等待管理员审核。
  
![](assets/08.png)   