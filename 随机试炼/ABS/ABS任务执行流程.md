# 通过ABS平台轻松胜任编译软件包的任务

在龙蜥社区官网活动页领取任务后，系统会自动创建一个任务（Issue），前往 Gitee 任务页查看任务内容，开始使用 ABS（Anolis Build Service）平台编译！

ABS 为用户提供了一站式的构建服务，通过 ABS 平台构建一个龙蜥社区软件包是十分方便快捷的，ABS平台为用户提供了个人空间、软件包搜索、镜像构建、软件包发布、软件仓库自动化创建等实用的功能，大大提升开发者在操作系统研发过程中的效率，统一的构建方式也减少了由于构建环境差异导致的错误。

假设，你抽到了“编译pam软件包”的任务，本文档以此为例进行指导。

## 操作步骤

### 第一步：记录你的任务 ID

在开始执行任务前，在 Gitee 任务页查看并记录任务 ID（即**IssueID**），后续提 PR 时需要使用此 ID。

![image.png](./assets/abs-task-001.png)

### 第二步：将官方仓库 fork 到你的本地仓库

1. 当你领取任务后，Gitee 任务页中已经提供了对应的软件包地址，点击进入软件包的源码仓库。

   你也可以在 <a href="https://gitee.com/src-anolis-os" target="_blank">Anolis OS代码仓库</a> 中搜索对应的软件包，并进入该仓库。例如，下图以 pam 软件包为例进行了搜索。
   ![](./assets/abs-task-002.png)

2. 在开始开发之前，必须对软件包进行 fork 操作。

    Fork 操作会将当前软件包复制一份到开发者仓库中，这样开发者进行修改时就不会影响源位置的代码。

    2.1 点击**Fork**。
    
    ![](./assets/abs-task-003.png) 

    2.2 在弹出的对话框中，选择你的个人空间，并点击**确认**。

    ![](./assets/abs-task-004.png)

    2.3 Fork 操作完成后会跳转到如下位置，点击**克隆/下载**按钮，然后点击**复制**保存你的个人仓库URL，下面的步骤中将会使用它。

    说明：这个仓库看起来与刚才的仓库相似，但是左上角有“forked from …..”字样，代表这个仓库是派生于一个“上游”仓库。

    ![](./assets/abs-task-005.png)

### 第三步：登录 ABS 平台

1. 前往 <a href='https://gitee.com/link?target=https%3A%2F%2Fabs.openanolis.cn%2Fall_project' target='_blank'>ABS平台</a> 。

2. 登录 ABS。 <br />![](./assets/new-abs-task-006.png)

3. 登录以后，进入个人空间。  <br />![](./assets/new-abs-task-007.png)

### 第四步：创建一个软件包项目

1. 在个人空间中，新建一个软件包项目。
    项目是软件包的集合，也可以理解为是软件包的容器
    ![新建项目](./assets/new-abs-task-009.png)

2. 填写信息后完成创建。 
    ![](./assets/new-abs-task-010.png)

3. 在公开项目列表中找到这个你的项目，并点击进入项目页。
    ![](./assets/new-abs-task-011.png)

### 第五步：添加软件包

1. 在项目页中，点击**添加软件包**。
    ![](./assets/new-abs-task-012.png)

2. 将“第二步”中复制的个人仓库链接，粘贴到这里。

    注意，分支名称（Branch名称）默认输入**a8**，以便于在 Anolis OS 8 系统上进行软件包的编译。

    ![image.png](./assets/new-abs-task-013.png)

### 第六步：开始构建软件包

当添加好一个软件包后，软件包构建就可以开始了，不过在正式开始之前，需要确认配置是否正确。

1. 配置仓库。在**个人空间**页面的右上角，点击**项目配置**。在弹窗中，**产品类型**选择**Anolis 8**，**架构类型**默认都勾选。

    ![image.png](./assets/new-abs-task-014_1.png)

    ![image.png](./assets/new-abs-task-014_2.png)

2. 配置后，点击**构建**。

    ![](./assets/new-abs-task-015.png)

3. 进入pam软件包仓库，查看对应的软件包编译进度和详情。

    ![](./assets/new-abs-task-016.png)

    ![image.png](./assets/new-abs-task-017.png)

当软件包编译好以后会生成对应的软件包仓库，只要依据配置写好，就可以完成软件包仓库配置，并开始使用阁下亲手编译的软件包。

![](./assets/new-abs-task-018.png)

## 下一步
截至目前，你完成了一个软件包的编译，但还未完全完成领取的任务，**必须提交PR，才能完成任务，获得贡献值。** 请前往<a href="https://gitee.com/anolis-challenge/summer2022/blob/master/%E9%9A%8F%E6%9C%BA%E8%AF%95%E7%82%BC/ABS/ABS%E4%BB%BB%E5%8A%A1%E9%AA%8C%E6%94%B6%E6%B5%81%E7%A8%8B.md" target="_blank">下一步</a>了解<a href="https://gitee.com/anolis-challenge/summer2022/blob/master/%E9%9A%8F%E6%9C%BA%E8%AF%95%E7%82%BC/ABS/ABS%E4%BB%BB%E5%8A%A1%E9%AA%8C%E6%94%B6%E6%B5%81%E7%A8%8B.md" target="_blank">如何完成提交PR</a>。

## 联系我们

虽然通过 ABS 平台构建软件包似乎比较简单、方便，但是，事实上底层基础实现还是非常复杂的，如果在构建时出现编译失败，或者使用问题，可以钉钉扫码咨询**_Anolis OS发行版SIG组_**，也可以加入**_openAnolis社区交流群_**沟通交流。

![image.png](./assets/abs-task-019.png) 

![image.png](./assets/abs-task-020.png)
